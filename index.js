let firstName = 'Von', lastName = 'Arceo';

console.log("First Name: " + firstName)
console.log("Last Name: " + lastName)

let age = 12;

console.log("Age: " + age);

let hobbies = [ "Eating", "Sleeping", "Studying" ];
console.log("Hobbies: ")
console.log(hobbies);

let workAddress = {
	houseNumber: '7888 building 2',
		street: 'Field Residences',
		city: 'Paranaque',
		state: 'Metro Manila'
};

console.log("Work Address: ");
console.log(workAddress);

let fullName = 'Steve Rogers';
console.log("My Fullname is: " + fullName);

console.log("My Current Age is: " + age);
console.log("My Friends are: ")

let friends = ['Tony', 'Bruce', 'Thor', 'Natasha', 'Clint', 'Nick' ];
console.log(friends);

console.log("My Full Profile:");

let isActive = false

let fullProfile = {
	username: 'im95yearsold',
	fullName: 'Steve Rogers',
	age: 95,
	isActive: false

};
console.log(fullProfile);

let bestFriend = 'Bucky Barnes';

console.log("My Best Friend is: " + bestFriend);

let found = 'Swimming Pool';

console.log("I was found frozen in: " + found)
